import 'package:flutter/material.dart';
import 'package:navigation_flutter/welcome_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Pass and Return Value for Flutter',
      theme: ThemeData(
        primarySwatch: Colors.blueGrey,
      ),
     home: const MyHomePage(title: 'Passing Value 0030'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  TextEditingController _name = TextEditingController();
  TextEditingController _email = TextEditingController();
  TextEditingController _phone = TextEditingController();

  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        //title: Text(widget.title),
        title: Text('Passing Value 0030'),
        backgroundColor: Colors.blueAccent,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          /*children: <Widget>[
            const Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headline4,
            ),
          ],*/
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                controller: _name,
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Enter Your Full Name: '),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                controller: _email,
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Enter Your Email Address: '),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                controller: _phone,
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Enter Your Phone Number: '),
              ),
            ),
            ElevatedButton(
                onPressed: () async {
                  _incrementCounter();
                  final result = await Navigator.of(context).push(
                    MaterialPageRoute(
                        builder: (context) => WelcomePage(
                          name: _name.text,
                          email: _email.text,
                          phone: _phone.text,
                        ),
                    ),
                  );
                  ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(
                          content: Text('$result, Press Button: $_counter'),
                      ),
                  );
                  if (result == 'You Selected Disagree') {
                    _name.clear();
                    _email.clear();
                    _phone.clear();
                  };
                },
              child: Text('Go Next Page',style: TextStyle(
                fontSize: 18, fontWeight: FontWeight.bold,
              ),),
            ),
          ],
        ),
      ),
    );
  }
}
