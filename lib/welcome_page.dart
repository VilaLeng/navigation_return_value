import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WelcomePage extends StatelessWidget {
  // const ({Key? key}) : super(key: key);
  String name, email, phone;

  WelcomePage(
      {Key? key, required this.name, required this.email, required this.phone})
      : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Returning Value 0030'),
        backgroundColor: Colors.blueGrey,
      ),
      body: Center(
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Data From Screen 1:',
                style: TextStyle(
                  fontSize: 30, fontWeight: FontWeight.bold, color: Colors.blueGrey,
                ),
              ),
              Text('Full Name: $name', style: TextStyle(
                fontSize: 15, fontWeight: FontWeight.bold, color: Colors.blueGrey,
              ),),
              Text('Email Address: $email',style: TextStyle(
                fontSize: 15, fontWeight: FontWeight.bold, color: Colors.blueGrey,
              ),),
              Text('Phone Number: $phone',style: TextStyle(
                fontSize: 15, fontWeight: FontWeight.bold, color: Colors.blueGrey,
              ),),
              Row(
                children: [
                  Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: ElevatedButton(
                        style: ButtonStyle(
                          backgroundColor:
                            MaterialStateProperty.all<Color>(Colors.blueGrey),
                        ),
                        onPressed: (){
                          Navigator.pop(context, 'You Selected Agree');
                        }, child: Text('Agree',style: TextStyle(
                        fontSize: 15, fontWeight: FontWeight.bold,
                      ),),
                      ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor:
                        MaterialStateProperty.all<Color>(Colors.blueGrey),
                      ),
                      onPressed: (){
                        Navigator.pop(context, 'You Selected Disagree');
                      }, child: Text('Disagree',style: TextStyle(
                      fontSize: 15, fontWeight: FontWeight.bold,
                    ),),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
